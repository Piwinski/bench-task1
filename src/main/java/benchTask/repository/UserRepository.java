package benchTask.repository;

import benchTask.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    User findByEmailOrUsernameAndPassword(String email, String username, String password);

    User findByEmailOrUsername(String email, String username);

    @Query(fields="{ 'email' : 1, 'username' : 1, 'id': 0}")
    List<User> findByEmailNotNull();

}
