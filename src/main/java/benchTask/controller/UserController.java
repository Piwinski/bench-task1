package benchTask.controller;

import benchTask.api.ErrorResponse;
import benchTask.api.UserRegistrationRequest;
import benchTask.model.User;
import benchTask.model.User.UserPublicView;
import benchTask.service.UserService;
import benchTask.utils.OnlineUserCache;
import com.fasterxml.jackson.annotation.JsonView;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.config.JmsListenerEndpointRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private OnlineUserCache onlineUserCache;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JmsListenerEndpointRegistry jmsListenerEndpointRegistry;


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(Model model) {
        model.addAttribute("name", "Chat");
        return "temp";
    }

    @RequestMapping(value = "/activate", method = RequestMethod.GET)
    public void activate(Model model) {
        jmsListenerEndpointRegistry.getListenerContainers().forEach(c -> c.start());
    }

    @RequestMapping(value = "/allUsers",  produces = "application/json", method = RequestMethod.GET)
    @JsonView(UserPublicView.class)
    public ResponseEntity<List<User>> findAllUsers() {
        List<User> users = userService.findAll();
        ResponseEntity<List<User>> response = new ResponseEntity<>(users, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/register", produces = "application/json", method = RequestMethod.POST)
    public ResponseEntity<User> register(@Valid @RequestBody UserRegistrationRequest request) {
        User user = modelMapper.map(request, User.class);
        User registereduser = userService.registerUser(user);
        return new ResponseEntity<>(registereduser, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/onlineUsers", produces = "application/json", method = RequestMethod.GET)
    @JsonView(UserPublicView.class)
    public ResponseEntity<List<User>> findOnlineUsers() {
        List<User> users = onlineUserCache.getUsers();
        ResponseEntity<List<User>> response = new ResponseEntity<>(users, HttpStatus.OK);
        return response;
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ErrorResponse handleException(MethodArgumentNotValidException e) {
        //TODO fix this message
        String message = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return new ErrorResponse(HttpStatus.BAD_REQUEST, message);
    }
}
