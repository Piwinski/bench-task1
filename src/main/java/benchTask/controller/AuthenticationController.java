package benchTask.controller;

import benchTask.api.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jms.config.JmsListenerEndpointRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class AuthenticationController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/start", method = RequestMethod.GET)
    public String login(Model model) {
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model) {
        return "exit";
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String errorPage(Model model) {
        return "error";
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ErrorResponse handleException(MethodArgumentNotValidException e) {
        //TODO fix this message
        String message = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return new ErrorResponse(HttpStatus.BAD_REQUEST, message);
    }

}
