package benchTask.model;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.Collection;

public class UserAuthentication implements Authentication{

    private final User user;
    private final OAuth2AccessToken token;
    private boolean autheticated = true;

    public UserAuthentication(User user, OAuth2AccessToken token) {
        this.user = user;
        this.token = token;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getAuthorities();
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public User getDetails() {
        return user;
    }

    @Override
    public Object getPrincipal() {
        return user.getUsername();
    }

    @Override
    public boolean isAuthenticated() {
        return autheticated;
    }

    @Override
    public void setAuthenticated(boolean autheticated) throws IllegalArgumentException {
        this.autheticated = autheticated;
    }

    public OAuth2AccessToken getToken() {
        return token;
    }

    @Override
    public String getName() {
        return user.getEmail();
    }
}
