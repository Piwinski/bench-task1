package benchTask.model;


public enum UserRole {
    USER, ADMIN;

    public UserAuthority asAuthority() {
        final UserAuthority authority = new UserAuthority();
        authority.setAuthority("ROLE_" + name());
        return authority;
    }

    public static UserRole valueOf(final UserAuthority authority) {
        switch (authority.getAuthority()) {
            case "ROLE_USER":
                return USER;
            case "ROLE_ADMIN":
                return ADMIN;
        }
        throw new IllegalArgumentException("No role defined for authority: " + authority.getAuthority());
    }
}