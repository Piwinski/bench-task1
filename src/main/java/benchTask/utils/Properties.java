package benchTask.utils;

public final class Properties {

//    public static final String BROKER_URL = "${app.broker.url}";
//    public static final String BROKER_NAME = "${app.broker.name}";

    public static final String ONLINE_USERS = "Chatter.Users.LoggedIn";
    public static final String BROKER_URL = "${jms.broker.url}";
    public static final String BROKER_NAME = "${jms.broker.name}";
    public static final String BROKER_USERNAME = "${jms.broker.user}";
    public static final String BROKER_PASSWORD = "${jms.broker.password}";

    //ActiveMQ
    public final static String MQ_MESSAGES_TOPIC = "Chatter.Users.Messages";
    public final static String MQ_LOGIN_TOPIC = "Chatter.Users.LoggedIn";

    private Properties() {}

}
/*
* Jms message (Not a JSON format. The Message format below is just an example. See: https://docs.oracle.com/cd/E23095_01/Platform.93/ATGProgGuide/html/s1102jmsmessageformats01.html for JMS message details):
Login request message:
{
	Headers: [
		JMSExpiration: 30s,
	],
	Properties: [
		username: "some_user_name",
		action: "ONLINE/OFFLINE"
	]
}

Messsage request message:
{
	Headers: [
		JMSDeliveryMode: PERSISTENT
	],
	Properties: [
		recipients: [recipent1, recipient2],		//always get it from received message and put it to your response message. If you created group, put group name and recipients to the message header. Filter messages by checking your user is in the recipients list.
		sender: "senderUserName"
	],
	TextMessage body: "message"

}
*
* */