package benchTask.utils;

public final class Constants {

    public static final long THIRTY_MINUTES = 30 * 60 * 1000L;

    public static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
    public static final String TEST_TOKEN = "BHDYTC%38&%DG(D^&D%EDG";
    public static final String EMAIL = "testEmail@op.pl";
    public static final String USERNAME = "mateusz";
    public static final String TMP_PASS = "password";

    public static final String AUTH_URL = "http://ecsc00100f77.epam.com:60080/oauth/token";
    public static final String ONLINE_USERS_URL = "http://ecsc00100f77.epam.com:60080/api/users/online";
    public static final String LOGOUT_URL = "http://ecsc00100f77.epam.com:60080/api/signout";


    public static final String PASSWORD_CLIENT_ID = "rest-client";
    public static final String PASSWORD_SECRET = "";
    public static final String CLIENT_CLIENT_ID = "rest-client-with-secret";
    public static final String CLIENT_SECRET = "strong_secret";

    public static final String SENDER = "sender";
    public static final String RECEIPIENTS = "recipients";

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String SOCKET_ID = "socketId";

    private Constants() {}

}
