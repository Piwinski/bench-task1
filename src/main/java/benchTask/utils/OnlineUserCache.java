package benchTask.utils;

import benchTask.model.User;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class OnlineUserCache {

    private Map<String, User> tokenUserMap;

    public OnlineUserCache() {
        tokenUserMap = Collections.synchronizedMap(new HashMap<>());
    }

    public User getUser(String token) {
        return tokenUserMap.get(token);
    }

    public void addUser(String token, User user) {
        tokenUserMap.put(token, user);
    }

    public List<User> getUsers() {
        return new ArrayList<>(tokenUserMap.values());
    }

    public boolean tokenExists(String token) {
        return tokenUserMap.containsKey(token);
    }

    public User removeToken(String token) {
        return tokenUserMap.remove(token);
    }

    public void removeExpiredTokens() {
        tokenUserMap.values().removeIf(user -> !user.isNotExpired());
    }

    public List<String> getSocketIds(List<String> receipients) {
        return tokenUserMap.values().stream()
                .filter(user -> receipients.contains(user.getUsername()))
                .map(u -> u.getSocketId()).collect(Collectors.toList());
    }
}
