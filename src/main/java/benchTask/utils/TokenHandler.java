package benchTask.utils;


import benchTask.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class TokenHandler {

    private static final String ALG = "HmacSHA256";
    private static final String SEPARATOR = ".";
    private static final String SEPARATOR_SPLITTER = "\\.";

    private final Mac hmac;

    public TokenHandler(byte[] secretKey) {
        try {
            hmac = Mac.getInstance(ALG);
            hmac.init(new SecretKeySpec(secretKey, ALG));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new IllegalStateException("failure initializing mac" + e.getMessage(), e);
        }
    }

    public String createToken(User user) {
        byte[] userData = toJSON(user);
        byte[] hash = createHmac(userData);
        final StringBuilder sb = new StringBuilder(170);
        sb.append(toBase64(userData));
        sb.append(SEPARATOR);
        sb.append(toBase64(hash));
        return sb.toString();
    }

    public User parseUserFromToken(String token) {
        String[] parts = token.split(SEPARATOR_SPLITTER);
        if (isValidTokenSyntax(parts)) {
            byte[] userData = fromBase64(parts[0]);
            byte[] hash = fromBase64(parts[1]);

            boolean validHash = Arrays.equals(hash, createHmac(userData));
            if (validHash) {
                final User user = fromJSON(userData);
                if (user.isNotExpired())
                    return user;
            }
        }
        return null;
    }

    private boolean isValidTokenSyntax(String[] parts) {
        return parts.length == 2 && parts[0].length() > 0 && parts[1].length() > 0;
    }

    private byte[] toJSON(User user) {
        try {
            return new ObjectMapper().writeValueAsBytes(user);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    private User fromJSON(final byte[] userData) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return objectMapper.readValue(new ByteArrayInputStream(userData), User.class);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private synchronized byte[] createHmac(byte[] content) {
        return hmac.doFinal(content);
    }

    private String toBase64(byte[] content) {
        return DatatypeConverter.printBase64Binary(content);
    }

    private byte[] fromBase64(String content) {
        return DatatypeConverter.parseBase64Binary(content);
    }
}
