package benchTask.utils;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

public class TokenTools {

    public static String parseToken(OAuth2AccessToken accessToken) {
        return accessToken.getTokenType() + " " + accessToken.getValue();
    }
}
