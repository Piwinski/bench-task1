package benchTask.config;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class FakeMongoConfig extends MongoConfig {

    @Override
    public Mongo mongo() throws Exception {
        return new Fongo("test server").getMongo();
    }
}
