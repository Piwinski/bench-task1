package benchTask.config;

import benchTask.repository.UserRepository;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Profile("default")
@Configuration
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
public class MongoConfig extends AbstractMongoConfiguration{

    @Value("${database.url}")
    private String url;

    @Value("${database.name}")
    private String databaseName;

    @Override
    protected String getDatabaseName() {
        return databaseName;
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient();
    }
}
