package benchTask.config;

import benchTask.utils.Properties;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.SimpleMessageConverter;


@Configuration
public class OnlineUsersConfig {

    @Value(Properties.BROKER_URL)
    private String brokerUrl;

    @Value(Properties.BROKER_USERNAME)
    private String brokerUsername;

    @Value(Properties.BROKER_PASSWORD)
    private String brokerPassword;

    @Bean
    @Qualifier("onlineUsersConnectionFactory")
    public ActiveMQConnectionFactory onlineUsersConnectionFactory() {
        return new ActiveMQConnectionFactory(brokerUsername, brokerPassword, brokerUrl);
    }

    /*@Bean
    public JmsTemplate onlineUsersJmsTemplate() {
        JmsTemplate jmsTemplate = new JmsTemplate();
        ActiveMQTopic topic = new ActiveMQTopic(Properties.ONLINE_USERS);
        jmsTemplate.setDefaultDestination(topic);
        jmsTemplate.setPubSubDomain(true);
        jmsTemplate.setConnectionFactory(onlineUsersConnectionFactory());
        return jmsTemplate;
    }*/

    @Bean
    public JmsListenerContainerFactory onlineUsersContainerFactory(@Qualifier("onlineUsersConnectionFactory") ActiveMQConnectionFactory onlineUsersConnectionFactory) {
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        factory.setConnectionFactory(onlineUsersConnectionFactory);
        factory.setClientId("usersListener");

        factory.setAutoStartup(false);

        factory.setSubscriptionDurable(true);
        factory.setPubSubDomain(true);
        factory.setMessageConverter(new SimpleMessageConverter());
        return factory;
    }
}
