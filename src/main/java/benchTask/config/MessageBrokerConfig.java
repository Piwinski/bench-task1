package benchTask.config;

import benchTask.utils.Properties;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.AbstractJmsListenerContainerFactory;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.AbstractMessageListenerContainer;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.SimpleMessageListenerContainer;

@EnableJms
@Configuration
public class MessageBrokerConfig {

    @Value(Properties.BROKER_URL)
    private String brokerUrl;

    @Value(Properties.BROKER_NAME)
    private String brokerName;

    @Value(Properties.BROKER_USERNAME)
    private String brokerUsername;

    @Value(Properties.BROKER_PASSWORD)
    private String brokerPassword;


    @Bean
    @Qualifier("connectionFactory")
    public ActiveMQConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory(brokerUsername, brokerPassword, brokerUrl);
    }

    @Bean
    public JmsTemplate jmsTemplate() {
        JmsTemplate jmsTemplate = new JmsTemplate();
        ActiveMQTopic topic = new ActiveMQTopic(brokerName);
        jmsTemplate.setDefaultDestination(topic);
        jmsTemplate.setPubSubDomain(true);
        jmsTemplate.setConnectionFactory(connectionFactory());
        return jmsTemplate;
    }

    @Bean
    public JmsListenerContainerFactory chatContainerFactory(@Qualifier("connectionFactory") ActiveMQConnectionFactory connectionFactory) {
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        factory.setAutoStartup(false);
        factory.setConnectionFactory(connectionFactory);
        factory.setClientId("chatListener");
        factory.setSubscriptionDurable(true);
        factory.setPubSubDomain(true);
        return factory;
    }

}
