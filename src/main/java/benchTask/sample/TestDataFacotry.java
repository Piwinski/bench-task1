package benchTask.sample;

import benchTask.api.Message;
import benchTask.utils.Constants;
import org.apache.activemq.command.ActiveMQTextMessage;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TestDataFacotry {

    public static TextMessage createValidTextMessage() throws JMSException {
        TextMessage message = new ActiveMQTextMessage();
        message.setStringProperty(Constants.SENDER, "sender");
        message.setObjectProperty(Constants.RECEIPIENTS, getNotEmptyStringList());
        message.setText("");
        return message;
    }

    public static Message createValidMessage() {
        Message message = new Message();
        message.setUsername("username");
        message.setSender("sender");
        message.setMessage("message");
        message.setToken("token");
        message.setRecipients("receipients");
        return message;
    }

    public static Message createInvalidMessage() {
        Message message = new Message();
        message.setUsername("username");
        message.setMessage("message");
        return message;
    }

    public static List<String> getNotEmptyStringList() {
        List<String> list = new ArrayList<>();
        list.add(UUID.randomUUID().toString());
        list.add(UUID.randomUUID().toString());
        list.add(UUID.randomUUID().toString());
        list.add(UUID.randomUUID().toString());
        list.add(UUID.randomUUID().toString());
        return list;
    }

}
