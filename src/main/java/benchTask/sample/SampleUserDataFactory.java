package benchTask.sample;

import benchTask.api.UserLoginRequest;
import benchTask.api.UserRegistrationRequest;
import benchTask.model.User;
import benchTask.model.UserRole;
import benchTask.utils.Constants;

public class SampleUserDataFactory {

    private SampleUserDataFactory() {};

    public static User getValidUser() {
        User user = new User();
        user.setEmail(Constants.EMAIL);
        user.setUsername(Constants.USERNAME);
        user.setPassword(Constants.TMP_PASS);
        user.grantRole(UserRole.USER);
        user.setExpires(System.currentTimeMillis() + Constants.THIRTY_MINUTES);
        return user;
    }

    public static User getExpiredUser() {
        User user = getValidUser();
        user.setExpires(System.currentTimeMillis());
        return user;
    }

    public static User getInvalidUser() {
        User user = new User();
        user.setUsername(Constants.USERNAME);
        user.setPassword(Constants.TMP_PASS);
        return user;
    }

    public static UserRegistrationRequest getRegistrationRequest() {
        UserRegistrationRequest request = new UserRegistrationRequest();
        request.setEmail(Constants.EMAIL);
        request.setUsername(Constants.USERNAME);
        request.setPassword(Constants.TMP_PASS);
        return request;
    }

    public static UserLoginRequest getValidLoginRequest() {
        UserLoginRequest request = new UserLoginRequest();
        request.setLogin(Constants.USERNAME);
        request.setPassword(Constants.TMP_PASS);
        return request;
    }

}
