package benchTask.oauth2;

import benchTask.api.UserLoginRequest;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static benchTask.utils.Constants.*;

public class Oauth2RestTemplateFactory {

    private static final List<String> CLIENT_SCOPE;
    private static final List<String> PASSWORD_SCOPE;

    static {
        CLIENT_SCOPE = new ArrayList<>();
        CLIENT_SCOPE.add("read");
        CLIENT_SCOPE.add("write");

        PASSWORD_SCOPE = new ArrayList<>();
        PASSWORD_SCOPE.add("read");
        PASSWORD_SCOPE.add("write");
        PASSWORD_SCOPE.add("trust");
    }

    public static OAuth2RestTemplate getPasswordRestTemplate(UserLoginRequest loginRequest) {
        ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();
        resource.setAccessTokenUri(AUTH_URL);
        resource.setClientId(PASSWORD_CLIENT_ID);
        resource.setClientSecret(PASSWORD_SECRET);
        resource.setScope(PASSWORD_SCOPE);
        resource.setClientAuthenticationScheme(AuthenticationScheme.header);
        resource.setUsername(loginRequest.getLogin());
        resource.setPassword(loginRequest.getPassword());

        AccessTokenRequest tokenRequest = new DefaultAccessTokenRequest();
        OAuth2RestTemplate template = new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext(tokenRequest));
        template.setAccessTokenProvider(new ResourceOwnerPasswordAccessTokenProvider());
        return template;
    }

    public static OAuth2RestTemplate getClientRestTemplate() {
        ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
        resource.setAccessTokenUri(AUTH_URL);
        resource.setClientId(CLIENT_CLIENT_ID);
        resource.setClientSecret(CLIENT_SECRET);
        resource.setScope(CLIENT_SCOPE);

        AccessTokenRequest request = new DefaultAccessTokenRequest();
        return new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext(request));
    }
}
