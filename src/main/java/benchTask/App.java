package benchTask;

import benchTask.security.TokenAuthenticationService;
import benchTask.utils.OnlineUserCache;
import benchTask.utils.TokenHandler;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;


@EnableScheduling
@EnableCaching
@SpringBootApplication
public class App {

    @Value("${app.token.secret}")
    private String secret;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public TokenAuthenticationService tokenAuthenticationService() {
        return new TokenAuthenticationService(tokenHandler(), onlineUserCache());
    }

    @Bean
    public TokenHandler tokenHandler() {
        return new TokenHandler(secret.getBytes());
    }

    @Bean
    public OnlineUserCache onlineUserCache() {
        return new OnlineUserCache();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(App.class, args);
        Arrays.toString(context.getBeanDefinitionNames());
    }

}
