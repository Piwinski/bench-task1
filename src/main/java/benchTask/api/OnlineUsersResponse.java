package benchTask.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class OnlineUsersResponse {

    private String message;
    private List<String> data;
}
