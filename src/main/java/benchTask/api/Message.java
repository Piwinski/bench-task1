package benchTask.api;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "token", doNotUseGetters = false)
@NoArgsConstructor
public class Message {

    private String message;
    private String token;
    private String username;
    private String recipients;
    private String sender;

    public Message(String message) {
        this.message = message;
    }

    public List<String> getListRecipients() {
        if (recipients == null)
            return null;
        return new ArrayList<>(Arrays.asList(recipients.split(",")));
    }
}
