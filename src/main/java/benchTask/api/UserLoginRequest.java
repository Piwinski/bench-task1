package benchTask.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginRequest {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;

}
