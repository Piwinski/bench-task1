package benchTask.security;

import benchTask.utils.Constants;
import benchTask.utils.OnlineUserCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TokenExpireJob {

    @Autowired
    private OnlineUserCache onlineUserStorage;

    public TokenExpireJob() {}

    @Scheduled(fixedDelay = Constants.THIRTY_MINUTES)
    public void removeExpiredTokens() {
        onlineUserStorage.removeExpiredTokens();
    }

}
