package benchTask.security;

import benchTask.api.UserLoginRequest;
import benchTask.model.User;
import benchTask.model.UserAuthentication;
import benchTask.service.UserService;
import benchTask.utils.OnlineUserCache;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    private TokenAuthenticationService tokenAuthenticationService;
    private UserService userService;
    private OnlineUserCache onlineUserStorage;

    public LoginFilter(String defaultFilterProcessesUrl, TokenAuthenticationService tokenAuthenticationService,
                          UserService userService, AuthenticationManager authenticationManager,
                          OnlineUserCache onlineUserStorage) {
        super(new AntPathRequestMatcher(defaultFilterProcessesUrl));
        this.tokenAuthenticationService = tokenAuthenticationService;
        this.userService = userService;
        this.onlineUserStorage = onlineUserStorage;
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
                                                HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        final UserLoginRequest loginRequest = new ObjectMapper().readValue(httpServletRequest.getInputStream(), UserLoginRequest.class);
        UsernamePasswordAuthenticationToken loginToken =  new UsernamePasswordAuthenticationToken(loginRequest.getLogin(), loginRequest.getPassword());
        return getAuthenticationManager().authenticate(loginToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authentication) throws IOException, ServletException {
        final User authenticatedUser = userService.loadUserByUsername(authentication.getName());
        final UserAuthentication userAuthentication = new UserAuthentication(authenticatedUser, null);

        String token = tokenAuthenticationService.addAuthentication(response, userAuthentication);
        onlineUserStorage.addUser(token, authenticatedUser);
        SecurityContextHolder.getContext().setAuthentication(userAuthentication);
        response.sendRedirect("/after");
//        chain.doFilter(request, response);
    }
}
