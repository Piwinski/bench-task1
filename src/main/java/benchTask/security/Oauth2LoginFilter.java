package benchTask.security;

import benchTask.api.UserLoginRequest;
import benchTask.model.User;
import benchTask.model.UserAuthentication;
import benchTask.service.AuthenticationService;
import benchTask.utils.OnlineUserCache;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static benchTask.utils.Constants.*;
import static benchTask.utils.TokenTools.parseToken;

public class Oauth2LoginFilter extends OAuth2ClientAuthenticationProcessingFilter {

    private OnlineUserCache onlineUserCache;
    private AuthenticationService authenticationService;

    public Oauth2LoginFilter(String defaultFilterProcessesUrl, OnlineUserCache onlineUserCache, RemoteTokenServices tokenServices, AuthenticationService authenticationService) {
        super(defaultFilterProcessesUrl);
        this.onlineUserCache = onlineUserCache;
        this.authenticationService = authenticationService;
        this.setTokenServices(tokenServices);
    }

    @Override
    public UserAuthentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        OAuth2RestTemplate restTemplate = authenticationService.login(request);
        OAuth2AccessToken accessToken = restTemplate.getAccessToken();
        setRestTemplate(restTemplate);
        UserLoginRequest loginRequest = buildUserLoginRequest(request);
        UserAuthentication authentication = createUserAuthentication(accessToken, loginRequest);
        response.addCookie(new Cookie(AUTH_HEADER_NAME, parseToken(accessToken)));
        return authentication;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
        OAuth2AccessToken token = (OAuth2AccessToken) authentication.getCredentials();

        final User user = new User();
        final String socketId = UUID.randomUUID().toString();
        response.addCookie(new Cookie(SOCKET_ID, socketId));

        user.setUsername(authentication.getName());
        user.setExpires(System.currentTimeMillis() + token.getExpiresIn());
        user.setSocketId(socketId);
        onlineUserCache.addUser(parseToken(token), user);
        super.successfulAuthentication(request, response, chain, authentication);
    }

    private UserAuthentication createUserAuthentication(OAuth2AccessToken accessToken, UserLoginRequest loginRequest) {
        User user = new User();
        user.setUsername(loginRequest.getLogin());
        user.setEmail(loginRequest.getLogin());
        user.setExpires(accessToken.getExpiration().getTime());
        return new UserAuthentication(user, accessToken);
    }

    private UserLoginRequest buildUserLoginRequest(HttpServletRequest request) {
        UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(request.getParameter(LOGIN));
        loginRequest.setPassword(request.getParameter(PASSWORD));
        return loginRequest;
    }
}
