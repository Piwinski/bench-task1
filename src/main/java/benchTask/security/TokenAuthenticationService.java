package benchTask.security;

import benchTask.model.User;
import benchTask.model.UserAuthentication;
import benchTask.utils.Constants;
import benchTask.utils.OnlineUserCache;
import benchTask.utils.TokenHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenAuthenticationService {

    private OnlineUserCache onlineUserCache;
    private TokenHandler tokenHandler;

    @Autowired
    public TokenAuthenticationService(TokenHandler tokenHandler, OnlineUserCache onlineUserCache) {
        this.tokenHandler = tokenHandler;
        this.onlineUserCache = onlineUserCache;
    }

    public String addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
        final User user = authentication.getDetails();
        user.setExpires(System.currentTimeMillis() + Constants.THIRTY_MINUTES);
        String token = tokenHandler.createToken(user);
        response.addHeader(Constants.AUTH_HEADER_NAME, token);
        return token;
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        final String token = request.getHeader(Constants.AUTH_HEADER_NAME);
        if (isValidToken(token)) {
            final User user = tokenHandler.parseUserFromToken(token);
            if (user.isNotExpired()) {
                return new UserAuthentication(user, null);
            }
        }
        return null;
    }

    private boolean isValidToken(String token) {
        return token != null && onlineUserCache.tokenExists(token);
    }
}