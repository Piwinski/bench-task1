package benchTask.service;

import benchTask.api.OnlineUsersResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import javax.servlet.http.HttpServletRequest;

public interface AuthenticationService {

    OAuth2AccessToken clientToken();

    OAuth2RestTemplate login(HttpServletRequest request);

    void logout(HttpServletRequest request);

    OnlineUsersResponse usersOnline();
}
