package benchTask.service;

import benchTask.api.OnlineUsersResponse;
import benchTask.api.UserLoginRequest;
import benchTask.oauth2.Oauth2RestTemplateFactory;
import benchTask.utils.Constants;
import benchTask.utils.OnlineUserCache;
import benchTask.utils.TokenTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Service
public class AuthenticationServiceImp implements AuthenticationService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OnlineUserCache onlineUserCache;

    @Override
    public OAuth2AccessToken clientToken() {
        return Oauth2RestTemplateFactory.getClientRestTemplate().getAccessToken();
    }

    @Override
    public OAuth2RestTemplate login(HttpServletRequest request) {
        UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(request.getParameter("login"));
        loginRequest.setPassword(request.getParameter("password"));
        OAuth2RestTemplate template = Oauth2RestTemplateFactory.getPasswordRestTemplate(loginRequest);
        return template;
    }

    @Override
    public void logout(HttpServletRequest request) {
        String token = getTokenFromCookie(request.getCookies());
        restTemplate.exchange(Constants.LOGOUT_URL, HttpMethod.GET, prepareRequest(token), String.class);
        onlineUserCache.removeToken(token);
    }

    @Override
    public OnlineUsersResponse usersOnline() {
        OAuth2AccessToken accessToken = Oauth2RestTemplateFactory.getClientRestTemplate().getAccessToken();
        String token = TokenTools.parseToken(accessToken);
        return restTemplate.exchange(Constants.ONLINE_USERS_URL, HttpMethod.GET, prepareRequest(token), OnlineUsersResponse.class).getBody();
    }

    private String getTokenFromCookie(Cookie[] cookies) {
        return Arrays.stream(cookies).filter(c -> Constants.AUTH_HEADER_NAME.equals(c.getName())).findFirst().get().getValue();
    }

    private HttpEntity<String> prepareRequest(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        return new HttpEntity<>(headers);
    }
}
