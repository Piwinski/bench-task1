package benchTask.service;

import benchTask.model.User;
import benchTask.model.UserRole;
import benchTask.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User registerUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.grantRole(UserRole.USER);
        return repository.save(user);
    }

    @Override
    public List<User> findAll() {
        return repository.findByEmailNotNull();
    }

    @Override
    public User loadUserByUsername(String value) throws UsernameNotFoundException {
        return repository.findByEmailOrUsername(value, value);
    }
}
