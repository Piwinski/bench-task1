package benchTask.service;

import benchTask.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService extends UserDetailsService {

    User registerUser(User userDto);

    List<User> findAll();

    @Override
    User loadUserByUsername(String value) throws UsernameNotFoundException;
}
