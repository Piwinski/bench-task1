package benchTask.messaging;

import benchTask.api.Message;
import benchTask.model.User;
import benchTask.utils.Constants;
import benchTask.utils.OnlineUserCache;
import benchTask.utils.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Session;

@Controller
public class MessageSender {

    @Value(Properties.BROKER_NAME)
    private String brokerName;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private OnlineUserCache onlineUserCache;

    @MessageMapping("/chat/send")
    public void sendMessage(final Message message) {
        if (isValidMessage(message)) {
            User user = onlineUserCache.getUser(message.getToken());
            message.setUsername(user.getUsername());
            jmsTemplate.send(brokerName, createMessage(message));
        } else
            throw new IllegalArgumentException("Wrong message format");
    }

    private MessageCreator createMessage(Message message) {
          return new MessageCreator() {
              @Override
              public javax.jms.Message createMessage(Session session) throws JMSException {
                  javax.jms.TextMessage textMessage = session.createTextMessage();
                  textMessage.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
                  textMessage.setObjectProperty(Constants.RECEIPIENTS, message.getListRecipients());
                  textMessage.setStringProperty(Constants.SENDER, message.getUsername());
                  textMessage.setText(message.getMessage());
                  return textMessage;
              }
          };
    }

    private boolean isValidMessage(Message message) {
        return message.getSender() != null && message.getToken() != null && message.getRecipients() != null;
    }
}
