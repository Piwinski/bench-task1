package benchTask.messaging;

import benchTask.api.Message;
import benchTask.api.OnlineUsersResponse;
import benchTask.service.AuthenticationService;
import benchTask.utils.Constants;
import benchTask.utils.OnlineUserCache;
import benchTask.utils.Properties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.util.List;

@Slf4j
@Component
public class MessageReceiver {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private OnlineUserCache onlineUserCache;

    @JmsListener(destination = Properties.BROKER_NAME, containerFactory = "chatContainerFactory")
    public void receiveMessage(TextMessage message) throws JMSException {
        log.info("Received message: " + message.getText());
        List<String> receipients = (List) message.getObjectProperty(Constants.RECEIPIENTS);
        if (receipients == null) {
            throw new IllegalArgumentException("Receipients parameter is required");
        }
        List<String> sokcetIds = onlineUserCache.getSocketIds(receipients);
        Message stompMessage = parseMessage(message);
        sokcetIds.forEach(id -> messagingTemplate.convertAndSend("/chat/receive/" + id, stompMessage));
    }

    @JmsListener(destination = Properties.ONLINE_USERS, containerFactory = "onlineUsersContainerFactory")
    public void receiveUserAction(TextMessage message) throws Exception {
        updateOnlineUsers();
    }

    @Scheduled(fixedDelay = 10 * 1000L)
    public void updateOnlineUsers() {
        OnlineUsersResponse users = authenticationService.usersOnline();
        messagingTemplate.convertAndSend("/chat/online", users);
    }

    private Message parseMessage(TextMessage message) throws JMSException {
        Message messageJson = new Message();
        messageJson.setSender(message.getStringProperty("sender"));
        messageJson.setMessage(message.getText());
        return messageJson;
    }
}
