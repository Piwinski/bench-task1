var stompClient = null;

function connect() {
        var socketId = $.cookie("socketId");
        var socket = new SockJS('/chat/send');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function(frame) {
            setConnected(true);
            console.log('Connected: ' + frame);
            stompClient.subscribe('/chat/receive/' + socketId, function(message){
                showMessage(JSON.parse(message.body).sender, JSON.parse(message.body).message);
            });
            var test = $.get( "/activate", function() {
                console.log("active listener");
            })
        });

        var usersSocket = new SockJS('/chat/send');
            stompUsersClient = Stomp.over(usersSocket);
            stompUsersClient.connect({}, function(frame) {
                setConnected(true);
                console.log('Connected: ' + frame);
                stompUsersClient.subscribe('/chat/online', function(message){
                    updateUsersList(JSON.parse(message.body).data);
                });
            });
    };

$(document).ready(function() {
    connect();
});

function setConnected(connected) {
    document.getElementById('connect').disabled = connected;
    document.getElementById('disconnect').disabled = !connected;
    document.getElementById('conversationDiv').style.visibility = connected ? 'visible' : 'hidden';
    document.getElementById('response').innerHTML = '';
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
        stompUsersClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendMessage() {
    var name = document.getElementById('name').value;
    $("#name").val('');
    var clientToken = $.cookie("X-AUTH-TOKEN");
    var recipients = $('#receipient').val();
    stompClient.send("/chat/send", {}, JSON.stringify({ 'message': name, "token": clientToken, "recipients": recipients}));
}

function showMessage(sender, message) {
    var response = document.getElementById('response');
    var p = document.createElement('p');
    p.style.wordWrap = 'break-word';
    p.appendChild(document.createTextNode(sender + " : " + message));
    response.appendChild(p);
    $('#response').scrollTop($('#response').height());
}

function updateUsersList(users) {
    $("#receipient").html("");
    $("#receipient").append("<option value=\""+ users + "\"> ALL </option>");
    $.each( users, function( key, user ) {
        $("#receipient").append("<option value=\""+ user +"\">"+ user +"</option>");
    })
    $("#usersList").empty();
    $("#usersList").text(users);
}