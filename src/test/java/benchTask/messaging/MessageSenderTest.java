package benchTask.messaging;

import benchTask.utils.OnlineUserCache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import static benchTask.sample.SampleUserDataFactory.getValidUser;
import static benchTask.sample.TestDataFacotry.createInvalidMessage;
import static benchTask.sample.TestDataFacotry.createValidMessage;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageSenderTest {

    @Mock
    private JmsTemplate jmsTemplate;

    @Mock
    private OnlineUserCache onlineUserCache;

    @InjectMocks
    private MessageSender messageSender;

    @Test
    public void testSendMessage_ShouldSendValidMessage() {
        when(onlineUserCache.getUser(anyString())).thenReturn(getValidUser());

        messageSender.sendMessage(createValidMessage());

        verify(jmsTemplate, times(1)).send(anyString(), any(MessageCreator.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSendMessage_WrongMessageFormat_ShouldThrowException() {
        messageSender.sendMessage(createInvalidMessage());
    }
}
