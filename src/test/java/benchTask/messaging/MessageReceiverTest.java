package benchTask.messaging;

import benchTask.api.Message;
import benchTask.api.OnlineUsersResponse;
import benchTask.service.AuthenticationService;
import benchTask.utils.Constants;
import benchTask.utils.OnlineUserCache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.util.List;

import static benchTask.sample.TestDataFacotry.createValidTextMessage;
import static benchTask.sample.TestDataFacotry.getNotEmptyStringList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageReceiverTest {

    @Mock
    private SimpMessagingTemplate messagingTemplate;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private OnlineUserCache onlineUserCache;

    @InjectMocks
    private MessageReceiver receiver;

    private TextMessage message;

    @Before
    public void setUp() throws JMSException {
        message = createValidTextMessage();
    }

    @Test
    public void testReceiveMessage_ShouldSendMessageToSocket() throws JMSException {
        List<String> socketIds = getNotEmptyStringList();
        when(onlineUserCache.getSocketIds(anyListOf(String.class))).thenReturn(socketIds);

        receiver.receiveMessage(message);

        verify(messagingTemplate, times(socketIds.size())).convertAndSend(anyString(), any(Message.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReceiveMessage_MissingReceipients_ShouldThrowException() throws JMSException {
        message = mock(TextMessage.class);

        when(message.getObjectProperty(Constants.RECEIPIENTS)).thenReturn(null);

        receiver.receiveMessage(message);
    }

    @Test
    public void testReceiveUserAction_ShouldUpdateUsersList() throws Exception {
        receiver = spy(new MessageReceiver());
        doNothing().when(receiver).updateOnlineUsers();

        receiver.receiveUserAction(message);

        verify(receiver, times(1)).updateOnlineUsers();
    }

    @Test
    public void testUpdateOnlineUsers() {
        when(authenticationService.usersOnline()).thenReturn(new OnlineUsersResponse());

        receiver.updateOnlineUsers();

        verify(messagingTemplate, times(1)).convertAndSend(anyString(), any(OnlineUsersResponse.class));
    }


}