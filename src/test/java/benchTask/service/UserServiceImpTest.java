package benchTask.service;

import benchTask.App;
import benchTask.model.User;
import benchTask.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

import static benchTask.sample.SampleUserDataFactory.getValidUser;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Profile("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = App.class, loader = SpringApplicationContextLoader.class)
@ComponentScan
@WebAppConfiguration
public class UserServiceImpTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testRegisterUser_ValidUser_ShouldCreateUser() {
        User user = getValidUser();

        given(userRepository.save(user)).willReturn(getValidUser());

        User createdUser = userService.registerUser(user);

        assertEquals(user.getEmail(), createdUser.getEmail());
    }

    @Test
    public void testFindAll_NoUsers_ShouldReturnEmptyList() {
        given(userRepository.findByEmailNotNull()).willReturn(new ArrayList<User>());

        List<User> result = userService.findAll();

        assertEquals(0, result.size());
    }

}
