package benchTask.service;

import benchTask.api.OnlineUsersResponse;
import benchTask.api.UserLoginRequest;
import benchTask.oauth2.Oauth2RestTemplateFactory;
import benchTask.sample.SampleUserDataFactory;
import benchTask.utils.Constants;
import benchTask.utils.OnlineUserCache;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Oauth2RestTemplateFactory.class)
public class AuthenticationServiceImpTest {

    private Oauth2RestTemplateFactory restTemplateFactory;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private OnlineUserCache onlineUserCache;

    @InjectMocks
    private AuthenticationService authenticationService = new AuthenticationServiceImp();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Oauth2RestTemplateFactory.class);
    }

    @Test
    public void testClientToken() {
        OAuth2RestTemplate oAuth2RestTemplate = mock(OAuth2RestTemplate.class);
        OAuth2AccessToken accessToken = mock(OAuth2AccessToken.class);

        when(restTemplateFactory.getClientRestTemplate()).thenReturn(oAuth2RestTemplate);
        when(oAuth2RestTemplate.getAccessToken()).thenReturn(accessToken);

        OAuth2AccessToken token = authenticationService.clientToken();

        Assert.assertNotNull(accessToken);
    }

    @Test
    public void testLogin_ShouldReturnAccessToken() {
        OAuth2RestTemplate oAuth2RestTemplate = mock(OAuth2RestTemplate.class);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("login", "login");
        request.addParameter("password", "password");

        when(restTemplateFactory.getPasswordRestTemplate(any(UserLoginRequest.class))).thenReturn(oAuth2RestTemplate);

        OAuth2RestTemplate restTemplate = authenticationService.login(request);

        Assert.assertNotNull(restTemplate);
    }

    @Test
    public void testLogout_ValidRequest_ShouldRemoveUserFromOnlineUsersList() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(new Cookie(Constants.AUTH_HEADER_NAME, ""));

        when(onlineUserCache.removeToken(anyString())).thenReturn(SampleUserDataFactory.getValidUser());

        authenticationService.logout(request);

        verify(restTemplate, times(1))
                .exchange(anyString(),
                        Mockito.<HttpMethod> any(),
                        Mockito.<HttpEntity<?>> any(),
                        Mockito.<Class<?>> any());

        verify(onlineUserCache, times(1)).removeToken(anyString());
    }

    @Test(expected = NoSuchElementException.class)
    public void testLogout_MissingTokenCookie_ShouldThrowException() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(new Cookie("WRONG_COOKIE", ""));

        when(onlineUserCache.removeToken(anyString())).thenReturn(SampleUserDataFactory.getValidUser());

        authenticationService.logout(request);
    }

    @Test
    public void testUsersOnline() {
        ResponseEntity responseEntity = mock(ResponseEntity.class);
        OAuth2RestTemplate oAuth2RestTemplate = mock(OAuth2RestTemplate.class);
        OAuth2AccessToken accessToken = mock(OAuth2AccessToken.class);

        when(restTemplateFactory.getClientRestTemplate()).thenReturn(oAuth2RestTemplate);
        when(oAuth2RestTemplate.getAccessToken()).thenReturn(accessToken);

        when(restTemplate.exchange(
                anyString(),
                Mockito.<HttpMethod> any(),
                Mockito.<HttpEntity<?>> any(),
                Mockito.<Class<?>> any()))
                .thenReturn(responseEntity);

        OnlineUsersResponse response = authenticationService.usersOnline();

        verify(restTemplate, times(1))
                .exchange(anyString(),
                        Mockito.<HttpMethod> any(),
                        Mockito.<HttpEntity<?>> any(),
                        Mockito.<Class<?>> any());
    }
}
