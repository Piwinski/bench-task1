package benchTask.security;

import benchTask.messaging.MessageSender;
import benchTask.model.User;
import benchTask.model.UserAuthentication;
import benchTask.utils.OnlineUserCache;
import benchTask.utils.TokenHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;

import java.lang.reflect.Field;

import static benchTask.sample.SampleUserDataFactory.getExpiredUser;
import static benchTask.sample.SampleUserDataFactory.getValidUser;
import static benchTask.utils.Constants.AUTH_HEADER_NAME;
import static benchTask.utils.Constants.TEST_TOKEN;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TokenHandler.class)
public class TokenAuthenticationServiceTest {

    @Mock
    private OnlineUserCache onlineUserCache;

    @Mock
    private TokenHandler tokenHandler;

    private TokenAuthenticationService tokenAuthenticationService;

    @Before
    public void setUp() {
        tokenAuthenticationService = new TokenAuthenticationService(tokenHandler, onlineUserCache);
    }

    @Test
    public void testAddAuthentication_ValidRequest_ShouldAddToken() {
        UserAuthentication authentication = new UserAuthentication(getValidUser(), null);
        MockHttpServletResponse response = new MockHttpServletResponse();

        when(tokenHandler.createToken(any(User.class))).thenReturn(anyString());

        tokenAuthenticationService.addAuthentication(response, authentication);

        assertTrue(response.containsHeader(AUTH_HEADER_NAME));
    }

    @Test
    public void testGetAuthentication_ValidToken_ShouldGetAuthentication() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(AUTH_HEADER_NAME, TEST_TOKEN);

        when(onlineUserCache.tokenExists(anyString())).thenReturn(true);
        when(tokenHandler.parseUserFromToken(anyString())).thenReturn(getValidUser());

        Authentication authentication = tokenAuthenticationService.getAuthentication(request);
        assertTrue(authentication.isAuthenticated());
    }

    @Test
    public void testGetAuthentication_ExpiredToken_ShouldReturnNull() throws NoSuchFieldException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(AUTH_HEADER_NAME, TEST_TOKEN);

        when(onlineUserCache.tokenExists(anyString())).thenReturn(true);
        when(tokenHandler.parseUserFromToken(anyString())).thenReturn(getExpiredUser());

        Authentication authentication = tokenAuthenticationService.getAuthentication(request);
        assertNull(authentication);
    }

}
