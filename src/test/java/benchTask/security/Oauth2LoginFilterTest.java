package benchTask.security;

import benchTask.model.User;
import benchTask.model.UserAuthentication;
import benchTask.service.AuthenticationService;
import benchTask.utils.OnlineUserCache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;

import static benchTask.utils.Constants.AUTH_HEADER_NAME;
import static benchTask.utils.Constants.SOCKET_ID;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class Oauth2LoginFilterTest {

    @Mock
    private OnlineUserCache onlineUserCache;

    @Mock
    private RemoteTokenServices remoteTokenServices;

    @Mock
    private AuthenticationService authenticationService;

    @InjectMocks
    private Oauth2LoginFilter loginFilter = new Oauth2LoginFilter("/login", onlineUserCache, remoteTokenServices, authenticationService);

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private OAuth2AccessToken accessToken;
    private OAuth2RestTemplate oAuth2RestTemplate;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        accessToken = mock(OAuth2AccessToken.class);
        oAuth2RestTemplate = mock(OAuth2RestTemplate.class);
    }

    @Test
    public void testAttemptAuthentication() throws IOException, ServletException {
        when(authenticationService.login(any(HttpServletRequest.class))).thenReturn(oAuth2RestTemplate);
        when(oAuth2RestTemplate.getAccessToken()).thenReturn(accessToken);
        when(accessToken.getExpiration()).thenReturn(new Date());

        UserAuthentication userAuthentication = loginFilter.attemptAuthentication(request, response);

        assertNotNull(response.getCookie(AUTH_HEADER_NAME));
        assertNotNull(userAuthentication);
    }

    @Test
    public void testSuccessfulAuthentication() throws IOException, ServletException {
        Authentication authentication = mock(Authentication.class);
        FilterChain filterChain = mock(FilterChain.class);

        loginFilter.setRestTemplate(oAuth2RestTemplate);
        when(authentication.getCredentials()).thenReturn(accessToken);

        loginFilter.successfulAuthentication(request, response, filterChain, authentication);

        verify(onlineUserCache, times(1)).addUser(anyString(), any(User.class));
        assertNotNull(response.getCookie(SOCKET_ID));
    }

}
