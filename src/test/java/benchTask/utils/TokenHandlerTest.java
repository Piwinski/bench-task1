package benchTask.utils;

import benchTask.model.User;
import org.junit.Before;
import org.junit.Test;

import java.security.SecureRandom;

import static benchTask.sample.SampleUserDataFactory.*;
import static org.junit.Assert.*;

public class TokenHandlerTest {

    private TokenHandler tokenHandler;

    @Before
    public void setUp() {
        byte[] secret = new byte[100];
        new SecureRandom().nextBytes(secret);
        tokenHandler = new TokenHandler(secret);
    }

    @Test
    public void testCreateToken_ValidUser_ShouldReturnToken() {
        User user = getValidUser();
        String token = tokenHandler.createToken(user);
        assertNotNull("Token cannot be null", token);
    }

    @Test
    public void testParseUserFromToken_ValidToken_ShouldReturnUser() {
        User sampleUser = getValidUser();
        User parsedUser = tokenHandler.parseUserFromToken(tokenHandler.createToken(sampleUser));
        assertEquals(sampleUser.getUsername(), parsedUser.getUsername());
    }

    @Test
    public void testParseUserFromToken_ExpiredToken_ShouldReturnNull() {
        User sampleUser = getExpiredUser();
        User parsedUser = tokenHandler.parseUserFromToken(tokenHandler.createToken(sampleUser));
        assertNull(parsedUser);
    }
}
