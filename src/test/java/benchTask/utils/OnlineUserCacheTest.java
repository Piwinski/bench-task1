package benchTask.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.UUID;
import static org.junit.Assert.*;
import static benchTask.sample.SampleUserDataFactory.*;

public class OnlineUserCacheTest {

    private OnlineUserCache onlineUserStorage;

    @Before
    public void setUp() {
        this.onlineUserStorage = new OnlineUserCache();
        onlineUserStorage.addUser(UUID.randomUUID().toString(), getValidUser());
        onlineUserStorage.addUser(UUID.randomUUID().toString(), getExpiredUser());
    }

    @Test
    public void testRemoveExpiredTokens_ShouldRemoveToken() {
        onlineUserStorage.removeExpiredTokens();
        assertEquals(1, onlineUserStorage.getUsers().size());
    }
}
